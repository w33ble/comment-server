/* eslint-disable */
const fs = require('fs');
const faker = require('faker');

const wstream = fs.createWriteStream('sample_data.json');
const docCount = 10000;

const posts = [
  'post-e6baf23c-8624-47c4-b9ce-e2506fe1e7ce',
  'post-ec65b607-443c-4670-9577-b42278775eac',
  'post-600e800d-7e96-4979-ac8e-169e920ebf86',
  'post-ee860db9-e6c3-4ec0-b549-6ed24979b143',
  'post-2a71c746-a181-453d-9312-5488bf10baaf',
];

wstream.write('{ "comments": [\n');

for (let i = 0; i < docCount; i += 1) {
  const end = (i === (docCount - 1)) ? '' : ',';
  const rec = faker.fake(
    '{'
    + `"id":"${faker.random.uuid()}",`
    + `"time":"${faker.date.past()}",`
    + `"name":"${faker.name.firstName()} ${faker.name.lastName()}",`
    + `"email":"${faker.internet.email()}",`
    + `"comment":"${faker.lorem.paragraph()}\\n\\n${faker.lorem.paragraph()}",`
    + `"published":${Boolean(Math.round(Math.random() * 100) % 3)},`
    + `"post":"${posts[Math.round(Math.random() * posts.length)]}"`
    + '}'
    + end);
  wstream.write(`${rec}\n`);
}

wstream.write(']}');
wstream.end();
