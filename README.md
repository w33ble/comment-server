# comment-server

Simple comment API server.

[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://raw.githubusercontent.com/w33ble/comment-server/master/LICENSE)
[![Build Status](https://travis-ci.org/w33ble/comment-server.svg?branch=master)](https://travis-ci.org/w33ble/comment-server)
[![Coverage](https://img.shields.io/codecov/c/github/w33ble/comment-server.svg)](https://codecov.io/gh/w33ble/comment-server)
[![npm](https://img.shields.io/npm/v/comment-server.svg)](https://www.npmjs.com/package/comment-server)
[![Project Status](https://img.shields.io/badge/status-experimental-orange.svg)](https://nodejs.org/api/documentation.html#documentation_stability_index)

## Plans

- As this is inspired by [isso](https://posativ.org/isso/), consider following [its api](https://posativ.org/isso/docs/extras/api/)
- The whole thing is based on a json file, and powered by [lowdb](https://github.com/typicode/lowdb)
  - Putting a cache in place might help with speed as more and more comments are created, [catbox](https://github.com/hapijs/catbox) looks like an interesting choice

#### License

MIT © [w33ble](https://github.com/w33ble)