/* eslint-disable no-console */
const low = require('lowdb');
const FileAsync = require('lowdb/adapters/FileAsync');

const adapter = new FileAsync('sample_data.json');

const start = new Date();
low(adapter).then(db => db.get('comments')
  .filter({ published: true, post: 'post-600e800d-7e96-4979-ac8e-169e920ebf86' })
  .sortBy('name')
  .take(50)
  .value() // eslint-disable-line comma-dangle
).then((docs) => {
  const end = new Date();
  console.log(docs);
  console.log(`found: ${docs.length}`);
  console.log(`took: ${end.getTime() - start.getTime()}ms`);
});
